#include <ArduinoJson.h>
#include "DHT.h"
#include <Wire.h>
#include <BH1750.h>
#define DHTTYPE DHT11 // DHT 11

//PIN Sensor Luz
BH1750 Luxometro;

//PIN Sensor Temperatura
uint8_t DHTPin = 2;
DHT dht(DHTPin, DHTTYPE);

void setup() 
{
  
  Serial.begin(115200);
  delay(100);
  
  //Serial.println("-- INIT SYSTEM --");
  
  pinMode(DHTPin, INPUT);
  dht.begin();
  
  Wire.begin();
  Luxometro.begin();

}

void loop() 
{
  while(Serial.available())
  {
    String dataS = Serial.readStringUntil('\r');
    ProcesaSerial(dataS);
  }
}

void ProcesaSerial(String data)
{
  StaticJsonDocument<256> doc;
  deserializeJson(doc, data);
  SendSerialData();
}

void SendSerialData()
{
  float temp = dht.readTemperature(); // Gets the values of the temperature
  float hum = dht.readHumidity(); // Gets the values of the humidity
  float lux = Luxometro.readLightLevel(); // Extrae Luminosidad

  StaticJsonDocument<200> jData;
  char buffer[200];
  jData["Accion"] = "getData";
  jData["PayLoad"]["Temperatura"] = temp;
  jData["PayLoad"]["Humedad"] = hum;
  jData["PayLoad"]["Lux"] = lux;
  serializeJson(jData, buffer);
  Serial.println(buffer);
}
