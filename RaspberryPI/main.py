#!/usr/bin/python3

import serial
import time
import json
import sys
import cv2
import logging
import ntplib
from time import ctime,localtime
from PIL import ImageFont, ImageDraw, Image, ImageColor
import numpy as np
import requests
import base64
import os

###################### PARAMETROS ESTACION ###################### 
NOMBRE_ESTACION = "MAIPU"
SEGUNDOS_ENTRE_CAPTURAS = 10
ENVIA_A_LA_NUBE = True
URL_GOOGLE_SCRIPTS = "https://script.google.com/macros/s/AKfycbxy6YQjodYWbXS08LpyAKn5bP9GrLVWn_DNpr2yTCO-clUn56yK/exec"

###################### PARAMETROS CAMARA ###################### 
CAP_PROP_FRAME_WIDTH  = 2592.0
CAP_PROP_FRAME_HEIGHT = 1944.0
CAP_PROP_BRIGHTNESS   = 50.0
CAP_PROP_CONTRAST     = 0.0
CAP_PROP_SATURATION   = 0.0
CAP_PROP_EXPOSURE     = 300.0
CAP_PROP_GAIN         = -1.0
CAP_PROP_HUE          = -1.0

###################### PARAMETROS SISTEMA ######################  
SERIAL_BAUD = 115200
SERIAL_PORT = "/dev/ttyUSB0"
RUTA_HOME = "/home/pi/Desktop/eclipsestation"
RUTA_CAPTURAS = f"{RUTA_HOME}/capturas"
RUTA_ERRORES  = f"{RUTA_HOME}/errores"
RUTA_LOG = f"{RUTA_HOME}/estacion.log"
RUTA_FUENTE = f"{RUTA_HOME}/RaspberryPI/roboto.ttf"     

###################### PARAMETROS MONITOR LOCAL ######################
MONITOR_CAP_PROP_FRAME_WIDTH  = 480
MONITOR_CAP_PROP_FRAME_HEIGHT = 320

################################# LOG #################################
FORMAT = "%(asctime)-15s %(message)s"
logging.basicConfig(format=FORMAT, filename=RUTA_LOG, level=logging.DEBUG,datefmt='%Y-%m-%d %H:%M:%S')
logger = logging.getLogger()
################################# LOG #################################

def RunMonitor():
    while True:
        logger.info(f"### INICIO ENVÍO MONITOREO ###")
        esOk, path_imagen = captura_y_envia_datos()
        logger.info(f"###[{path_imagen}] Respuesta: {esOk}")
        logger.info(f"### FIN ENVÍO MONITOREO ###")
        print(f"#-> [{path_imagen}] Respuesta: {esOk}")
        muestra_imagen(path_imagen, esOk, SEGUNDOS_ENTRE_CAPTURAS)


def muestra_imagen(path, esOK, segundos):
    radio = 200    
    borde = 100
    coordenadas = (int(CAP_PROP_FRAME_WIDTH)-radio-borde ,int(CAP_PROP_FRAME_HEIGHT)-radio-borde)
    if esOK:
        color = (0, 255, 0) #verde
    else:
        color = (0, 0, 255) #rojo
    
    ruta_final = f"{RUTA_CAPTURAS}/{path}"
    cv2.namedWindow("MONITOR", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("MONITOR", MONITOR_CAP_PROP_FRAME_WIDTH, MONITOR_CAP_PROP_FRAME_HEIGHT)
    img_monitor = cv2.imread(ruta_final)
    img_monitor = cv2.circle(img_monitor, coordenadas, radio, color, -1)
    cv2.imshow('MONITOR', img_monitor)
    cv2.waitKey(segundos*1000)


def captura_y_envia_datos():
    imagen = captura_imagen()
    jdata = getDataSensors()
    temp = jdata["PayLoad"]["Temperatura"]
    lux = jdata["PayLoad"]["Lux"]
    ffile, fimg, fgoo = obtiene_hora()
    imagen = agrega_info_imagen(imagen,
                                NOMBRE_ESTACION,
                                fimg,
                                temp,
                                lux)
    
    path_imagen = f"IMG_{ffile}.jpg" 
    logger.info(f"Guarda Imagen : {RUTA_CAPTURAS}/{path_imagen}, T: {temp}, L: {lux}")
    cv2.imwrite(f"{RUTA_CAPTURAS}/{path_imagen}", imagen)
    esOk = enviar_data_a_la_nube(path_imagen, NOMBRE_ESTACION, fgoo, temp, lux)
    return esOk, path_imagen


def getDataSensors():
    logger.info(f"Serial -> Ingreso")
    while True:
        try:
            esp8266 = serial.Serial(SERIAL_PORT, SERIAL_BAUD)
            logger.info(f"Serial -> Puerto Abierto")
            esp8266.write(b'DATA')
            logger.info(f"Serial -> Envio Solicitud")
            rawString = esp8266.readline()
            logger.info(f"Serial -> Recepciono data")
            json_data = json.loads(rawString)
            logger.info(f"Serial -> Serializo data {json_data}")
            esp8266.close()
            logger.info(f"Serial -> Cierro puerto")
            return json_data
        except:
            e = sys.exc_info()[0]
            logger.error(f"SE HA PRODUCIDO UN ERROR AL CAPTURAR DATA SENSORES / REINTENTANDO [{e}]")
            time.sleep(1)


def captura_imagen():
    logger.info(f"Captura -> Inicio")
    cap = cv2.VideoCapture(-1)
    logger.info(f"Captura -> Camara Abierta")
    if cap.isOpened():
        
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, CAP_PROP_FRAME_WIDTH)
        logger.info(f"CAP_PROP_FRAME_WIDTH: {cap.get(cv2.CAP_PROP_FRAME_WIDTH)}")

        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, CAP_PROP_FRAME_HEIGHT)
        logger.info(f"CAP_PROP_FRAME_HEIGHT: {cap.get(cv2.CAP_PROP_FRAME_HEIGHT)}")
        
        cap.set(cv2.CAP_PROP_BRIGHTNESS,CAP_PROP_BRIGHTNESS)
        logger.info(f"CAP_PROP_BRIGHTNESS: {cap.get(cv2.CAP_PROP_BRIGHTNESS)}")
        
        cap.set(cv2.CAP_PROP_CONTRAST, CAP_PROP_CONTRAST)
        logger.info(f"CAP_PROP_CONTRAST: {cap.get(cv2.CAP_PROP_CONTRAST)}")
        
        cap.set(cv2.CAP_PROP_SATURATION,CAP_PROP_SATURATION) 
        logger.info(f"CAP_PROP_SATURATION: {cap.get(cv2.CAP_PROP_SATURATION)}")
        
        cap.set(cv2.CAP_PROP_EXPOSURE,CAP_PROP_EXPOSURE)
        logger.info(f"CAP_PROP_EXPOSURE: {cap.get(cv2.CAP_PROP_EXPOSURE)}")
        
        cap.set(cv2.CAP_PROP_GAIN,CAP_PROP_GAIN)
        logger.info(f"CAP_PROP_GAIN: {cap.get(cv2.CAP_PROP_GAIN)}")
        
        cap.set(cv2.CAP_PROP_HUE,CAP_PROP_HUE)
        logger.info(f"CAP_PROP_HUE: {cap.get(cv2.CAP_PROP_HUE)}")
        
        _, frame = cap.read()
        logger.info(f"Captura -> Frame Capturado")
        cap.release()
        logger.info(f"Captura -> Release")
        return frame


def ajusta_brillo_contraste(img):
    img_transf = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    clahe = cv2.createCLAHE(tileGridSize=(100,100))
    img_transf[:,:,2] = clahe.apply(img_transf[:,:,2])
    return cv2.cvtColor(img_transf, cv2.COLOR_HSV2BGR)


def agrega_info_imagen(imagen, lugar, hora, temp, lux):
    colorT = ImageColor.getrgb("#FFFF00")
    size = 150
    imagen = print_text(imagen, size, lugar, (50, 50), colorT)
    imagen = print_text(imagen, size, hora, (CAP_PROP_FRAME_WIDTH-900, 50), colorT)
    imagen = print_text(imagen, size, f"{temp} °C", (30, CAP_PROP_FRAME_HEIGHT-size-50-200), colorT)
    imagen = print_text(imagen, size, f"{int(lux)} lux", (30, CAP_PROP_FRAME_HEIGHT-200), colorT)
    return imagen

def print_text(img, font_size, text, org, color):
    font = ImageFont.truetype(RUTA_FUENTE, font_size)
    img_pil = Image.fromarray(img)
    draw = ImageDraw.Draw(img_pil)
    draw.text(org,  text, font=font, fill=color, embedded_color=False)
    return np.array(img_pil)


def enviar_data_a_la_nube(path_imagen, lugar, hora, temp, lux):
    if not ENVIA_A_LA_NUBE:
        msg_no_nube = f"Propiedad ENVIA_A_LA_NUBE en {cv2.ENVIA_A_LA_NUBE}, ¡¡ NO SE ESTÁ TRANSMITIENDO !!" 
        logger.info(msg_no_nube)
        print.info(msg_no_nube)
        return True

    jdata = None
    jdata["Lugar"] = lugar
    jdata["Hora"] = hora
    jdata["Temp"] = temp
    jdata["Lux"] = lux

    data = open(f"{RUTA_CAPTURAS}/{path_imagen}", "rb").read()
    encoded = base64.b64encode(data)
    
    payload = {
    'Imagen': (None, encoded),
    'Hora': (None, hora),
    'Lugar': (None, lugar),
    'Temp': (None, temp),
    'Lux': (None, lux),
    'Ruta': (None, path_imagen)
    }
    files=[]
    headers = {}
    try:
        response = requests.request("POST", URL_GOOGLE_SCRIPTS, headers=headers, data=payload, files=files)
        if (response.text.find("ARCHIVO RECIBIDO")>=0):
            return True
        else:
            logger.info(f"Error al procesar {path_imagen} -> {response.text}")
            guarda_para_reenvio(hora, lugar, temp, lux, path_imagen)
            return False
    except:
        logger.info(f"[CATCH] Error al procesar {path_imagen}")
        guarda_para_reenvio(hora, lugar, temp, lux, path_imagen)
        return False


def guarda_para_reenvio(hora, lugar, temp, lux, path_imagen):
    payload = { "Hora": hora,
    "Lugar": lugar,
    "Temp": temp,
    "Lux": lux,
    "Ruta": path_imagen
    }
    jsonStr = json.dumps(payload)
    path_json = path_imagen.replace(f".jpg","")+".json"
    err_file = open(f"{RUTA_ERRORES}/{path_json}", "w")
    err_file.write(jsonStr)
    err_file.close()

def obtiene_hora():
    try:
        tc = ntplib.NTPClient()
        response = tc.request('europe.pool.ntp.org', version=3)
        fec_file = time.strftime('%Y%m%d_%H%M%S',localtime(response.tx_time))
        fec_imagen = time.strftime('%H:%M:%S CLST',localtime(response.tx_time))
        fec_google = time.strftime('%H:%M:%S',localtime(response.tx_time))
        return fec_file, fec_imagen, fec_google
    except:
        fec_file = time.strftime('%Y%m%d_%H%M%S', localtime())
        fec_imagen = time.strftime('%H:%M:%S CLST', localtime())
        fec_google = time.strftime('%H:%M:%S', localtime())
        return fec_file, fec_imagen, fec_google
    
        
if __name__ == "__main__":
    RunMonitor()
