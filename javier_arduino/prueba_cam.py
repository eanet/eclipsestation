import cv2


print(f"Captura -> Inicio")
cap = cv2.VideoCapture(-1)
print(f"Captura -> Camara Abierta")
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
exitoso , frame = cap.read()
print(f"Captura -> Frame Capturado {exitoso}")
cap.release()
print(f"Captura -> Release")
cv2.imwrite(f"captura.jpg", frame)
#cv2.imshow('MONITOR', frame)
#cv2.waitKey(10000)


