// Incluimos librería
#include <DHT.h>
#include <ArduinoJson.h> // Se agrega esta otra libreria para sacar los datos al PC formateados

// Definimos el pin digital donde se conecta el sensor
#define DHTPIN 2
// Dependiendo del tipo de sensor
#define DHTTYPE DHT11
 
// Inicializamos el sensor DHT11
DHT dht(DHTPIN, DHTTYPE);
 
void setup() {
  // Inicializamos comunicación serie
  Serial.begin(115200);  //subi los baud para que sea mas rápida la comunicación
  delay(100);
  // Comenzamos el sensor DHT
  dht.begin();
 
}

void ProcesaSerial(String data)
{
  StaticJsonDocument<256> doc;   //Esta linea
  deserializeJson(doc, data);    // y esta otra tb  (pero no pesco el mensaje así que da lo mismo)
  SendSerialData();
}

//Aqui saco los datos de los sensores y los formateo para salir
void SendSerialData()
{
    float t = dht.readTemperature();
  // Leemos la temperatura en grados Fahreheit
    float f = dht.readTemperature(true);

    //esta es la variable que sirve para sacar la info
    StaticJsonDocument<200> jData;
    char buffer[200];
    
    if (isnan(t) || isnan(f)) 
    {
        //Saco el erro afuera
        jData["Accion"] = "Error";
        jData["PayLoad"]["Msg"] = "Error obteniendo los datos del sensor DHT11";
    }
    else
    {
        jData["Accion"] = "getData";
        jData["PayLoad"]["Temperatura"] = f;   //<- aqui mando tu dato de la temperatura
        jData["PayLoad"]["Temperatura2"] = t;   //<- aqui mando tu dato de la temperatura 2
    } 
    
    serializeJson(jData, buffer);
    Serial.println(buffer);
}

void loop() 
{
  //Cada vez que le mandes un mensaje a la placa con un salto de linea ella te debe responder
  while(Serial.available())
  {
    String dataS = Serial.readStringUntil('\r');
    ProcesaSerial(dataS);
  }
}