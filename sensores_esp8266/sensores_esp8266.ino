#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <Wire.h>
#include <BH1750.h>
#define DHTTYPE DHT11 // DHT 11

/*
Usar la generic ESP8266 Module
*/

/*Put your SSID & Password*/
const bool ConectaSSID = false; 
const char* ssid = "EADOMOTIC";      // Aqui va ti identificador
const char* password = "-Qwerty78-";  //Aqui va tu contraseña
ESP8266WebServer server(80);

//PIN Sensor Luz
BH1750 Luxometro;

//PIN Sensor Temperatura
uint8_t DHTPin = 2;
DHT dht(DHTPin, DHTTYPE);

void setup() 
{
  
  Serial.begin(115200);
  delay(100);
  
  //Serial.println("-- INIT SYSTEM --");
  
  pinMode(DHTPin, INPUT);
  dht.begin();
  
  Wire.begin();
  Luxometro.begin();
  
  if (ConectaSSID)
  {
    ConnectedWIFI();
  }
}

void ConnectedWIFI()
{
  
  Serial.println("-- INIT SYSTEM --");
  //Serial.println(ssid);
  SendSerialAction("Conectando", ssid, false,"");
  
  //connect to your local wi-fi network
  WiFi.begin(ssid, password);
  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(1000);
    SendSerialAction("EnEsperaConexion", ssid, false,"");
    //Serial.print(".");
  }
  
  SendSerialAction("Conectado", WiFi.localIP().toString().c_str(), false,"");

  server.on("/", handle_OnConnect);
  server.on("/json", handle_OnConnectJSON);
  server.onNotFound(handle_NotFound);

  server.begin();
  SendSerialAction("Iniciado", "OK", false,"");
}

void loop() 
{
  if (ConectaSSID)
  {
    server.handleClient();
  }
  while(Serial.available())
  {
    String dataS = Serial.readStringUntil('\r');
    ProcesaSerial(dataS);
  }
}

void ProcesaSerial(String data)
{
  StaticJsonDocument<256> doc;
  deserializeJson(doc, data);
   
  SendSerialData();
  
}


void handle_OnConnectJSON() { returnData("JSON"); }
void handle_OnConnect() { returnData("HTML"); }
void returnData(char* tipoData) {

  float Temperature = dht.readTemperature(); // Gets the values of the temperature
  float Humidity = dht.readHumidity(); // Gets the values of the humidity
  float Luminosidad = LuminosidadRead(); // Extrae Luminosidad
  if (tipoData=="JSON")
  {
    server.send(200, "text/json", genDataJson(Temperature,Humidity,Luminosidad));
  }
  else
  {
    server.send(200, "text/html", SendHTML(Temperature,Humidity,Luminosidad));
  }
}


float LuminosidadRead()
{
  float lux = Luxometro.readLightLevel();
  Serial.print(lux);
  return lux;
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}

String genDataJson(float temp,float hum,float lux)
{
  StaticJsonDocument<100> jData;
  jData["Temperatura"] = temp;
  jData["Humedad"] = hum;
  jData["Lux"] = lux;
  char buffer[100];
  serializeJson(jData, buffer);
  return buffer;
}

String genActionJson(const char* action,const char* value,const bool* EsError,const char* ErrorMsg)
{
  StaticJsonDocument<200> jData;
  jData["Accion"] = action;
  jData["Valor"] = value;
  jData["EsError"] = EsError;
  jData["ErrorMsg"] = ErrorMsg;
  char buffer[200];
  serializeJson(jData, buffer);
  return buffer;
}

void SendSerialAction(const char* action,const char* value,const bool* EsError,const char* ErrorMsg)
{
  Serial.println(genActionJson(action, value, EsError, ErrorMsg));
}
void SendSerialData()
{
  float temp = dht.readTemperature(); // Gets the values of the temperature
  float hum = dht.readHumidity(); // Gets the values of the humidity
  float lux = LuminosidadRead(); // Extrae Luminosidad

  StaticJsonDocument<200> jData;
  char buffer[200];
  jData["Accion"] = "getData";
  jData["PayLoad"]["Temperatura"] = temp;
  jData["PayLoad"]["Humedad"] = hum;
  jData["PayLoad"]["Lux"] = lux;
  serializeJson(jData, buffer);
  Serial.println(buffer);
}

String SendHTML(float Temperaturestat,float Humiditystat, float Luminosidadstat){
String ptr = "<!DOCTYPE html> <html lang=\"es\">\n";
ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
ptr +="<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>";
ptr +="<meta http-equiv=\"refresh\" content=\"1\">";
ptr +="<title>ESP8266 Weather Report</title>\n";
ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";
ptr +="p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";
ptr +="</style>\n";
ptr +="</head>\n";
ptr +="<body>\n";
ptr +="<div id=\"webpage\">\n";
ptr +="<h1>Sensores Terminal Eclipse ALPERIT</h1>\n";

ptr +="<p>Temperature: ";
ptr +=(int)Temperaturestat;
ptr +="°C</p>";
ptr +="<p>Humidity: ";
ptr +=(int)Humiditystat;
ptr +="%</p>";
ptr +="<p>Luminosidad: ";
ptr +=(int)Luminosidadstat;
ptr +=" lux</p>";

ptr +="</div>\n";
ptr +="</body>\n";
ptr +="</html>\n";
return ptr;
}
