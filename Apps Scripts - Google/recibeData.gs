function doPost(e) {
 
  Logger.log(e.parameter);
  
  var root_folder = "1PNKJeCpd4Tvjc36Quj5Jw51MopOcKAIZ"; // "Eclipse2020";
  
  var archivo = e.parameter.Ruta
  var Hora    = e.parameter.Hora;
  var Lugar   = e.parameter.Lugar;
  var Temp    = e.parameter.Temp;
  var Lux     = e.parameter.Lux;
  var Imagen  = e.parameter.Imagen;
  
  //Proceso data base64 como archivo binario nuevamente
  var data = Utilities.base64Decode(Imagen);
  var blob = Utilities.newBlob(data, MimeType.JPEG,archivo);

  var HoraRecepcion = Utilities.formatDate(new Date(), "GMT-3", "dd-MM-yyyy HH:mm:ss");
  
  //Obtengo carpeta root
  var folderRoot = DriveApp.getFolderById(root_folder);
  
  //Buscando la subcapeta del lugar 
  var subfolders = folderRoot.getFolders()
  var folderLugar = null;
  while (subfolders.hasNext()) 
  {
    var folder = subfolders.next();
    if (folder.getName()==Lugar)
    {
      folderLugar = folder; 
      Logger.log('Carpeta: '+folder.getName()+", ¡Encontrada!");
    }
  }
  
  //si no la encuentra la creo
  if (folderLugar == null)
  {
    folderLugar = folderRoot.createFolder(Lugar);
    Logger.log('Carpeta: '+folderLugar.getName()+", ¡Creada!");
  }
  
  //Creo el archivo en la carpeta Específica
  var newfile = folderLugar.createFile(blob);
  
  var urlDownload = newfile.getDownloadUrl()
  AddData(HoraRecepcion, Hora, Lugar, Temp, Lux, urlDownload);
   
  return ContentService.createTextOutput("ARCHIVO RECIBIDO ["+archivo+"]")
}

function AddData(fecha_recep, fecha_gen, lugar, temp, lux, file)
{
  var sheet_id = '1EW1lfSetfSKwxX3FB6sNVFrQtpdNugHh0_7OReb0r4c'; 		// Spreadsheet ID
  var sheet = SpreadsheetApp.openById(sheet_id).getActiveSheet();		// get Active sheet
  var newRow = sheet.getLastRow() + 1;						
  var rowData = [];
  rowData[0] = fecha_recep;
  rowData[1] = fecha_gen; 
  rowData[2] = lugar; 
  rowData[3] = temp.replace(".", ",");; 
  rowData[4] = lux.replace(".", ",");; 
  rowData[5] = file; 
  var newRange = sheet.getRange(newRow, 1, 1, rowData.length);
  newRange.setValues([rowData]);     
}
/**
* Remove leading and trailing single or double quotes
*/
function stripQuotes( value ) {
  return value.replace(/^["']|['"]$/g, "");
}